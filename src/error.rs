
#[derive(Debug)]
pub enum RatchetError {
    EncryptDecrypt,
    SerializeDeserialize,
    TooManyKeysSkipped,
    NoChainKeyAvailable,
    MessageNotFound,
    FirstReplyFromPeerNeeded
}

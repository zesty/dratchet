use sha2::Sha512;
use hkdf::Hkdf;
use x25519_dalek::SharedSecret;
#[cfg(test)]
use rand_core::OsRng;
#[cfg(test)]
use x25519_dalek::{StaticSecret, PublicKey};
use hmac::{Hmac, Mac};
use core::convert::TryInto;

pub fn kdf_rk(secret: &[u8; 32], dh_out: &SharedSecret) -> ([u8; 32], [u8; 32]) {
    let hasher = Hkdf::<Sha512>::new(Some(secret), dh_out.as_bytes());

    let mut okm = [0u8; 64];
    let info = b"Root Key Info";
    
    // Generating an output key material
    hasher.expand(info, &mut okm).unwrap();
    let (a, b) = okm.split_at(32);
    // a will be the root
    // b is a chain key (either sending or receiving)
    (a.try_into()
         .expect("Incorrect length"),
     b.try_into()
         .expect("Incorrect length"))
}

pub fn kdf_rk_he(secret: &[u8; 32], dh_out: &SharedSecret) -> ([u8; 32], [u8; 32], [u8; 32]) {
    let h = Hkdf::<Sha512>::new(Some(secret), dh_out.as_bytes());
    let mut okm = [0u8; 96];
    let info = b"Root Key Generator";
    h.expand(info, &mut okm).unwrap();
    let (rk, a) = okm.split_at(32);
    let (ck, nhk) = a.split_at(32);
    // rk: root key
    // nhk: next header key
    (
        rk.try_into().expect("Wrong length"),
        ck.try_into().expect("Wrong length"),
        nhk.try_into().expect("Wrong length")
    )
}

type HmacSha512 = Hmac<Sha512>;

pub fn kdf_ck(ck: &[u8; 32]) -> ([u8; 32], [u8; 32]) {
    let mac = HmacSha512::new_from_slice(ck)
        .expect("Invalid Key Length");
    let result = mac.finalize().into_bytes();
    let (a, b) = result.split_at(32);
    (a.try_into()
        .expect("Incorrect Length"),
    b.try_into()
        .expect("Incorrect Length"))
}

#[cfg(test)]
pub fn gen_shared_secret() -> SharedSecret {
    let alice_secret = StaticSecret::new(&mut OsRng);
    let alice_public = PublicKey::from(&alice_secret);

    let bob_secret = StaticSecret::new(&mut OsRng);
    let bob_public = PublicKey::from(&bob_secret);

    alice_secret.diffie_hellman(&bob_public)
}

#[cfg(test)]
pub fn gen_ck() -> [u8; 32] {
    let shared_secret = gen_shared_secret();
    let rk = [0; 32];
    let (_, ck) = kdf_rk(&rk, &shared_secret);
    ck
}

#[cfg(test)]
pub fn gen_mk() -> [u8; 32] {
    let ck = gen_ck();
    let (_, mk) = kdf_ck(&ck);
    mk
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn kdf_root_ratchet() {
        let rk = [0; 32];
        let shared_secret = gen_shared_secret();
        let (rk1, _)      = kdf_rk(&rk, &shared_secret);
        let (rk2, _)      = kdf_rk(&rk1, &shared_secret);
        assert_ne!(rk1, rk2)
    }

    #[test]
    fn kdf_chain_ratchet() {
        let ck = gen_ck();
        let (ck, mk1) = kdf_ck(&ck);
        let (_, mk2) = kdf_ck(&ck);
        assert_ne!(mk1, mk2)
    }
}

extern crate alloc;

use crate::error::RatchetError;
use alloc::vec::Vec;
use rand_core::{OsRng, RngCore};
use chacha20poly1305::{XChaCha20Poly1305, Key, aead::{KeyInit, AeadInPlace}, XNonce};

pub fn encrypt(secret: &[u8; 32], plaintext: &[u8], associated_data: &[u8]) -> Result<(Vec<u8>, [u8; 24]), RatchetError> {
    // Encrypting message
    let mut nonce = [0_u8; 24]; // Nonce size for XChaCha20Poly1305 is 24 bytes
    OsRng.fill_bytes(&mut nonce);
    
    let mut enctext = Vec::new();
    enctext.extend_from_slice(plaintext);

    let key     = Key::from_slice(secret);
    let cipher  = XChaCha20Poly1305::new(key);
    match cipher.encrypt_in_place(XNonce::from_slice(&nonce), associated_data, &mut enctext) {
        Ok(_) => (),
        Err(_) => return Err(RatchetError::EncryptDecrypt)
    }

    Ok((enctext, nonce))
}

pub fn decrypt(secret: &[u8; 32], enc_text: &[u8], associated_data: &[u8], nonce: &[u8; 24]) -> Result<Vec<u8>, RatchetError> {

    let mut plaintext = Vec::new();
    plaintext.extend_from_slice(enc_text);

    let key    = Key::from_slice(secret);
    let cipher = XChaCha20Poly1305::new(key);
    match cipher.decrypt_in_place(XNonce::from_slice(nonce), associated_data, &mut plaintext) {
        Ok(_) => (),
        Err(_) => return Err(RatchetError::EncryptDecrypt)
    }

    Ok(plaintext)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand_core::{OsRng, RngCore};

    #[test]
    fn enc_and_dec() {
        let test_data           = include_bytes!("aead.rs").to_vec();
        let associated_data     = include_bytes!("lib.rs").to_vec();
        let mut secret = [0_u8; 32];
        OsRng.fill_bytes(&mut secret);
        let (ciphertext, nonce) = encrypt(&secret, &test_data, &associated_data).unwrap();
        let plaintext           = decrypt(&secret, &ciphertext, &associated_data, &nonce).unwrap();
        assert_eq!(test_data, plaintext)
    }
}

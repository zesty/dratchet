extern crate alloc;

use chacha20poly1305::{XChaCha20Poly1305, Key, aead::{KeyInit, AeadInPlace}, XNonce};
use x25519_dalek::PublicKey;
use borsh::{BorshSerialize, BorshDeserialize};
use alloc::{vec::Vec, borrow::ToOwned};

use crate::{aead::encrypt, error::RatchetError};
#[cfg(test)]
use crate::key::DhKeyPair;

#[derive(Debug, Clone)]
pub struct Header {
    pub public_key: PublicKey,
    pub pn: usize, // Previous Chain Length
    pub n: usize, // Message Number
}

#[derive(Debug, BorshSerialize, BorshDeserialize)]
struct ExHeader {
    ad: Vec<u8>,
    public_key: [u8; 32],
    pn: usize,
    n: usize
}

impl Header {
    pub fn new(dh_pk: &PublicKey, pn: usize, n: usize) -> Self {
        Header {
            public_key: dh_pk.clone(),
            pn,
            n,
        }
    }

    pub fn concat(&self, ad: &[u8]) -> Result<Vec<u8>, RatchetError> {
        let ex_header = ExHeader {
            ad: ad.to_vec(),
            public_key: self.public_key.as_bytes().to_owned(),
            pn: self.pn,
            n: self.n
        };

        match ex_header.try_to_vec() {
            Ok(v) => Ok(v),
            Err(_) => Err(RatchetError::SerializeDeserialize)
        }
    }

    pub fn encrypt(&self, hk: &[u8; 32], ad: &[u8]) -> Result<(Vec<u8>, [u8; 24]), RatchetError> {
        let header_data = self.concat(ad)?;
        encrypt(hk, &header_data, b"")
    }

    pub fn decrypt(hk: &Option<[u8; 32]>, ciphertext: &[u8], nonce: &[u8; 24]) -> Result<Self, RatchetError> {
        let key_d = match hk {
            None => {
                return Err(RatchetError::EncryptDecrypt)
            },
            Some(d) => d
        };
        let key = Key::from_slice(key_d);
        let cipher = XChaCha20Poly1305::new(key);

        let nonce = XNonce::from_slice(nonce);
        let mut buffer = Vec::new();
        buffer.extend_from_slice(ciphertext);
        match cipher.decrypt_in_place(nonce, b"", &mut buffer) {
            Ok(_) => (),
            Err(_) => {
                return Err(RatchetError::EncryptDecrypt)
            }
        }
        Ok(Header::deser(&buffer)?)
    }

    pub fn ex_public_key_bytes(&self) -> Vec<u8> {
        self.public_key.as_bytes().to_vec()
    }

    pub fn deser(d: &[u8]) -> Result<Self, RatchetError> {
        let ex_header: ExHeader = match ExHeader::try_from_slice(d) {
            Ok(v) => v,
            Err(_) => return Err(RatchetError::SerializeDeserialize)
        };
        Ok(Header {
            public_key: PublicKey::from(ex_header.public_key),
            pn: ex_header.pn,
            n: ex_header.n,
        })
    }
}

impl PartialEq for Header {
    fn eq(&self, other: &Self) -> bool {
        if self.public_key == other.public_key
            && self.pn == other.pn
            && self.n == other.n {
            return true
        }
        false
    }
}

#[cfg(test)]
pub fn gen_header() -> Header {
    let dh_pair = DhKeyPair::new();
    let pn = 10;
    let n = 50;
    Header::new(&dh_pair.public, pn, n)
}


#[cfg(test)]
mod tests {
    extern crate alloc;
    use crate::header::{gen_header, Header, ExHeader};
    use crate::kdf::gen_mk;
    use crate::aead::{encrypt, decrypt};

    #[test]
    fn ser_des() {
        let ad = b"";
        let header = gen_header();
        let serialized = header.concat(ad).unwrap();
        let created = Header::deser(serialized.as_slice()).unwrap();
        assert_eq!(header, created)
    }

    #[test]
    fn enc_header() {
        let header = gen_header();
        let mk = gen_mk();
        let header_data = header.concat(b"").unwrap();
        let data = include_bytes!("aead.rs");
        let (encrypted, nonce) = encrypt(&mk, data, &header_data).unwrap();
        let decrypted = decrypt(&mk, &encrypted, &header_data, &nonce).unwrap();
        assert_eq!(decrypted, data.to_vec())
    }

    #[test]
    fn test_eq_header() {
        let header1 = gen_header();
        let header2 = gen_header();
        assert_ne!(header1, header2)
    }

    #[test]
    fn debug_header() {
        let header = gen_header();
        let _string = alloc::format!("{:?}", header);
    }

    #[test]
    fn gen_ex_header() {
        let ex_header = ExHeader {
            ad: alloc::vec![0],
            public_key: [0u8; 32],
            pn: 0,
            n: 0
        };
        let _string = alloc::format!("{:?}", ex_header);
    }
}

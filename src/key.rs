extern crate alloc;

use core::fmt::{self, Debug, Formatter};
use rand_core::OsRng;
use x25519_dalek::{StaticSecret, PublicKey, SharedSecret};

pub struct DhKeyPair {
    pub secret: StaticSecret,
    pub public: PublicKey
}

impl DhKeyPair {
    pub fn new() -> Self {
        let secret = StaticSecret::new(&mut OsRng);
        DhKeyPair {
            public: PublicKey::from(&secret),
            secret
        }
    }

    pub fn key_exchange(&self, other_pk: &PublicKey) -> SharedSecret {
        self.secret.diffie_hellman(other_pk)
    }

    /*pub fn pubkey_bytes(&self) -> Vec<u8> {
        self.public.as_bytes().to_vec()
    }*/
}

impl PartialEq for DhKeyPair {
    fn eq(&self, other: &Self) -> bool {
        if self.public.as_bytes() != other.public.as_bytes() {
            return false
        }
        true
    }
}

impl Debug for DhKeyPair {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("DhKeyPair")
            .field("public", self.public.as_bytes())
            .finish()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn key_generation() {
        let pair_1 = DhKeyPair::new();
        let pair_2 = DhKeyPair::new();
        assert_ne!(pair_1, pair_2)
    }

    #[test]
    fn key_exchange() {
        let alice_pair = DhKeyPair::new();
        let bob_pair   = DhKeyPair::new();
        let alice_shared_secret = alice_pair.key_exchange(&bob_pair.public);
        let bob_shared_secret   = bob_pair.key_exchange(&alice_pair.public);
        assert_eq!(alice_shared_secret.as_bytes(), bob_shared_secret.as_bytes())
    }

    #[test]
    fn test_format() {
        let key_pair = DhKeyPair::new();
        let _str = alloc::format!("{:?}", key_pair);
    }
}

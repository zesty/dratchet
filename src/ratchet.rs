extern crate alloc;

use crate::{
    key::DhKeyPair,
    kdf::{kdf_rk, kdf_ck, kdf_rk_he},
    header::Header,
    aead::{encrypt, decrypt}, error::RatchetError
};
use x25519_dalek::{PublicKey, StaticSecret};
use hashbrown::HashMap;
use alloc::vec::Vec;
use zeroize::Zeroize;
use borsh::{BorshSerialize, BorshDeserialize};

const MAX_SKIP: usize = 100;
type HeaderNonceCipherNonce = ((Vec<u8>, [u8; 24]), Vec<u8>, [u8; 24]);

/// Ratchet
pub struct Ratchet {
    /// Current key used for key exchange
    dhs: DhKeyPair,
    /// Other party diffie-hellman public key
    dhr: Option<PublicKey>,
    /// Root key
    rk: [u8; 32],
    /// Chain key for the receiving chain
    ckr: Option<[u8; 32]>,
    /// Chain key for the sending chain
    cks: Option<[u8; 32]>,
    /// Next message number for the sending chain
    ns: usize,
    /// Next message number for the sending chain
    nr: usize,
    /// Previous chain key, used for in header when sending message
    pn: usize,
    /// Some messages may get skipped due to size, network load...
    /// For now, they are stored in memory
    mkskipped: HashMap<(Vec<u8>, usize), [u8; 32]>, // TODO: Database?
}

impl Drop for Ratchet {
    fn drop(&mut self) {
        // dhs is sanitizes itself
        self.rk.zeroize();
        self.ckr.zeroize();
        self.cks.zeroize();
        self.ns.zeroize();
        self.nr.zeroize();
        self.pn.zeroize();
        self.mkskipped.clear();
    }
}

impl Ratchet {
    /// Init Ratchet with other [PublicKey]. Initialized second.
    pub fn init_alice(sk: [u8; 32], bob_dh_public_key: PublicKey) -> Self {
        let dhs = DhKeyPair::new();
        let (rk, cks) = kdf_rk(&sk,
                               &dhs.key_exchange(&bob_dh_public_key));
        Ratchet {
            dhs,
            dhr: Some(bob_dh_public_key),
            rk,
            cks: Some(cks),
            ckr: None,
            ns: 0,
            nr: 0,
            pn: 0,
            mkskipped: HashMap::new(),
        }
    }

    /// Init Ratchet without other [PublicKey]. Initialized first. Returns [Ratchet] and [PublicKey].
    pub fn init_bob(sk: [u8; 32]) -> (Self, PublicKey) {
        let dhs = DhKeyPair::new();
        let public_key = dhs.public;
        let ratchet = Ratchet {
            dhs,
            dhr: None,
            rk: sk,
            cks: None,
            ckr: None,
            ns: 0,
            nr: 0,
            pn: 0,
            mkskipped: HashMap::new(),
        };
        (ratchet, public_key)
    }

    /// Encrypt Plaintext with [Ratchet]. Returns Message [Header] and ciphertext.
    pub fn ratchet_encrypt(&mut self, plaintext: &[u8], ad: &[u8]) -> Result<(Header, Vec<u8>, [u8; 24]), RatchetError> {
        let (cks, mk) = kdf_ck(&match self.cks {
            Some(v) => v,
            None => return Err(RatchetError::FirstReplyFromPeerNeeded)
        });
        let header = Header::new(&self.dhs.public, self.pn, self.ns);
        let (encrypted_data, nonce) = encrypt(&mk, plaintext, &header.concat(ad)?)?;
        self.cks = Some(cks);
        self.ns += 1;
        Ok((header, encrypted_data, nonce))
    }

    fn try_skipped_message_keys(&mut self, header: &Header, ciphertext: &[u8], nonce: &[u8; 24], ad: &[u8]) -> Result<Option<Vec<u8>>, RatchetError> {
        match self.mkskipped.get(&(header.ex_public_key_bytes(), header.n)) {
            Some(mk) => {
                let data = decrypt(mk, ciphertext, &header.concat(ad)?, nonce)?;
                self.mkskipped.remove(&(header.ex_public_key_bytes(), header.n)).unwrap();
                Ok(Some(data))
            },
            None => Ok(None)
        }
    }

    fn skip_message_keys(&mut self, until: usize) -> Result<(), RatchetError> {
        if self.nr + MAX_SKIP < until {
            return Err(RatchetError::TooManyKeysSkipped);
        }
        match self.ckr {
            Some(mut d) => {
                while self.nr < until {
                    let (ckr, mk) = kdf_ck(&d);
                    self.ckr = Some(ckr);
                    d = ckr;
                    self.mkskipped.insert((self.dhr.unwrap().as_bytes().to_vec(), self.nr), mk);
                    self.nr += 1
                }
                Ok(())
            },
            None => { Err(RatchetError::NoChainKeyAvailable) }
        }
    }

    /// Decrypt ciphertext with ratchet. Requires Header. Returns plaintext.
    pub fn ratchet_decrypt(&mut self, header: &Header, ciphertext: &[u8], nonce: &[u8; 24], ad: &[u8]) -> Result<Vec<u8>, RatchetError> {
        let plaintext = self.try_skipped_message_keys(header, ciphertext, nonce, ad)?;
        match plaintext {
            Some(d) => Ok(d),
            None => {
                if Some(header.public_key) != self.dhr {
                    if self.ckr != None {
                        self.skip_message_keys(header.pn).unwrap();
                    }
                    self.dhratchet(header);
                }
                self.skip_message_keys(header.n).unwrap();
                let (ckr, mk) = kdf_ck(&self.ckr.unwrap());
                self.ckr = Some(ckr);
                self.nr += 1;
                Ok(decrypt(&mk, ciphertext, &header.concat(ad)?, nonce)?)
            }
        }
    }

    fn dhratchet(&mut self, header: &Header) {
        self.pn = self.ns;
        self.ns = 0;
        self.nr = 0;
        self.dhr = Some(header.public_key);
        let (rk, ckr) = kdf_rk(&self.rk, &self.dhs.key_exchange(&self.dhr.unwrap()));
        self.rk = rk;
        self.ckr = Some(ckr);
        self.dhs = DhKeyPair::new();
        let (rk, cks) = kdf_rk(&self.rk, &self.dhs.key_exchange(&self.dhr.unwrap()));
        self.rk = rk;
        self.cks = Some(cks);
    }
}

#[derive(PartialEq, Debug)]
pub struct RatchetEncHeader {
    dhs: DhKeyPair,
    dhr: Option<PublicKey>,
    rk: [u8; 32],
    cks: Option<[u8; 32]>,
    ckr: Option<[u8; 32]>,
    ns: usize,
    nr: usize,
    pn: usize,
    hks: Option<[u8; 32]>,
    hkr: Option<[u8; 32]>,
    nhks: Option<[u8; 32]>,
    nhkr: Option<[u8; 32]>,
    mkskipped: HashMap<(Option<[u8; 32]>, usize), [u8; 32]>
}

impl Zeroize for RatchetEncHeader {
    fn zeroize(&mut self) {
        self.rk.zeroize();
        self.cks.zeroize();
        self.ckr.zeroize();
        self.ns.zeroize();
        self.nr.zeroize();
        self.pn.zeroize();
        self.hks.zeroize();
        self.hkr.zeroize();
        self.nhks.zeroize();
        self.nhkr.zeroize();
        self.mkskipped.clear();

    }
}

impl Drop for RatchetEncHeader {
    fn drop(&mut self) {
        self.zeroize();
    }
}

#[derive(BorshSerialize, BorshDeserialize)]
struct ExRatchetEncHeader {
    dhs: ([u8; 32], [u8; 32]),
    dhr: Option<[u8; 32]>,
    rk: [u8; 32],
    cks: Option<[u8; 32]>,
    ckr: Option<[u8; 32]>,
    ns: usize,
    nr: usize,
    pn: usize,
    hks: Option<[u8; 32]>,
    hkr: Option<[u8; 32]>,
    nhks: Option<[u8; 32]>,
    nhkr: Option<[u8; 32]>,
    mkskipped: ExMkskipped
}

struct ExMkskipped(HashMap<(Option<[u8; 32]>, usize), [u8; 32]>);

impl BorshSerialize for ExMkskipped {
    #[inline]
    fn serialize<W: std::io::Write>(&self, writer: &mut W) -> std::io::Result<()> {
        let mut vec = self.0.iter().collect::<Vec<_>>();
        vec.sort_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap());
        u32::try_from(vec.len())
            .map_err(|_| std::io::ErrorKind::InvalidInput)?
            .serialize(writer)?;
        for (key, value) in vec {
            key.serialize(writer)?;
            value.serialize(writer)?;
        }
        Ok(())
    }
}

type Key = (Option<[u8; 32]>, usize);
type Val = [u8; 32];

impl BorshDeserialize for ExMkskipped {
    fn deserialize(buf: &mut &[u8]) -> std::io::Result<Self> {
        let len = u32::deserialize(buf)?;
        // TODO(16): return capacity allocation when we can safely do that.
        let mut result = HashMap::new();
        for _ in 0..len {
            let key = Key::deserialize(buf)?;
            let value = Val::deserialize(buf)?;
            result.insert(key, value);
        }
        Ok(ExMkskipped(result))
    }

    fn deserialize_reader<R: std::io::Read>(reader: &mut R) -> std::io::Result<Self> {
        let len = u32::deserialize_reader(reader)?;
        // TODO(16): return capacity allocation when we can safely do that.
        let mut result = HashMap::new();
        for _ in 0..len {
            let key = Key::deserialize_reader(reader)?;
            let value = Val::deserialize_reader(reader)?;
            result.insert(key, value);
        }
        Ok(ExMkskipped(result))
    }
}

impl From<&RatchetEncHeader> for ExRatchetEncHeader {
    fn from(reh: &RatchetEncHeader) -> Self {
        let private_dhs = reh.dhs.secret.to_bytes();
        let public_dhs = reh.dhs.public.to_bytes();
        let dhs = (private_dhs, public_dhs);
        let dhr = reh.dhr.map(|e| e.to_bytes());
        let rk = reh.rk;
        let cks = reh.cks;
        let ckr = reh.ckr;
        let ns = reh.ns;
        let nr = reh.nr;
        let pn = reh.pn;
        let hks = reh.hks;
        let hkr = reh.hkr;
        let nhks = reh.nhks;
        let nhkr = reh.nhkr;
        let mkskipped = reh.mkskipped.clone();
        Self {
            dhs,
            dhr,
            rk,
            cks,
            ckr,
            ns,
            nr,
            pn,
            hks,
            hkr,
            nhks,
            nhkr,
            mkskipped: ExMkskipped(mkskipped)
        }
    }
}

impl From<&ExRatchetEncHeader> for RatchetEncHeader {
    fn from(ex_reh: &ExRatchetEncHeader) -> Self {
        let private_dhs = StaticSecret::from(ex_reh.dhs.0);
        let public_dhs = PublicKey::from(ex_reh.dhs.1);
        let dhs = DhKeyPair {
            secret: private_dhs,
            public: public_dhs
        };
        let dhr = ex_reh.dhr.as_ref().map(|e| PublicKey::from(e.clone()));
        Self {
            dhs,
            dhr,
            rk: ex_reh.rk,
            cks: ex_reh.cks,
            ckr: ex_reh.ckr,
            ns: ex_reh.ns,
            nr: ex_reh.nr,
            pn: ex_reh.pn,
            hks: ex_reh.hks,
            hkr: ex_reh.hkr,
            nhks: ex_reh.nhks,
            nhkr: ex_reh.nhkr,
            mkskipped: ex_reh.mkskipped.0.clone()
        }
    }
}

impl RatchetEncHeader {
    pub fn init_alice(sk: [u8; 32],
                      bob_dh_public_key: PublicKey,
                      shared_hka: [u8; 32],
                      shared_nhkb: [u8; 32]) -> Self {
        let dhs = DhKeyPair::new();
        let (rk, cks, nhks) = kdf_rk_he(&sk, &dhs.key_exchange(&bob_dh_public_key));
        RatchetEncHeader {
            dhs,
            dhr: Some(bob_dh_public_key),
            rk,
            cks: Some(cks),
            ckr: None,
            ns: 0,
            nr: 0,
            pn: 0,
            mkskipped: HashMap::new(),
            hks: Some(shared_hka),
            hkr: None,
            nhkr: Some(shared_nhkb),
            nhks: Some(nhks),
        }
    }

    pub fn init_bob(sk: [u8; 32], shared_hka: [u8; 32], shared_nhkb: [u8; 32]) -> (Self, PublicKey) {
        let dhs = DhKeyPair::new();
        let public_key = dhs.public;
        let ratchet = Self {
            dhs,
            dhr: None,
            rk: sk,
            cks: None,
            ckr: None,
            ns: 0,
            nr: 0,
            pn: 0,
            mkskipped: HashMap::new(),
            hks: None,
            nhks: Some(shared_nhkb),
            hkr: None,
            nhkr: Some(shared_hka),
        };
        (ratchet, public_key)
    }

    pub fn ratchet_encrypt(&mut self, plaintext: &[u8], ad: &[u8]) -> Result<HeaderNonceCipherNonce, RatchetError> {
        let (cks, mk) = kdf_ck(&match self.cks {
            Some(v) => v,
            None => return Err(RatchetError::FirstReplyFromPeerNeeded)
        });
        let header = Header::new(&self.dhs.public, self.pn, self.ns);
        let enc_header = header.encrypt(&self.hks.unwrap(), ad)?;
        let encrypted = encrypt(&mk, plaintext, &header.concat(ad)?)?;
        self.cks = Some(cks);
        self.ns += 1;

        Ok((enc_header, encrypted.0, encrypted.1))
    }

    fn try_skipped_message_keys(&mut self, enc_header: &(Vec<u8>, [u8; 24]),
                                ciphertext: &[u8], nonce: &[u8; 24], ad: &[u8]) -> Result<(Vec<u8>, Header), RatchetError> {

        let mut ret_data = None;
        for e in self.mkskipped.clone().into_iter() {
            let header = Header::decrypt(&e.0.0, &enc_header.0, &enc_header.1);
            match header {
                Err(_) => (),
                Ok(h) => {
                    if h.n == e.0.1 {
                        ret_data = Some(e);
                    }
                }
            }
        }

        match ret_data {
            None => { Err(RatchetError::MessageNotFound) },
            Some(data) => {
                let header = Header::decrypt(&data.0.0, &enc_header.0, &enc_header.1)?;
                let mk = data.1;
                self.mkskipped.remove(&(data.0.0, data.0.1));
                Ok((decrypt(&mk, ciphertext, &header.concat(ad)?, nonce)?, header))
            }
        }
    }

    fn decrypt_header(&mut self, enc_header: &(Vec<u8>, [u8; 24])) -> Result<(Header, bool), RatchetError> {
        let header = Header::decrypt(&self.hkr, &enc_header.0, &enc_header.1);
        if let Ok(h) = header { return Ok((h, false)) };
        let header = Header::decrypt(&self.nhkr, &enc_header.0, &enc_header.1);
        match header {
            Ok(h) => Ok((h, true)),
            Err(_) => Err(RatchetError::EncryptDecrypt)
        }
    }

    fn skip_message_keys(&mut self, until: usize) -> Result<(), RatchetError> {
        if self.nr + MAX_SKIP < until {
            return Err(RatchetError::TooManyKeysSkipped)
        }
        if let Some(d) = &mut self.ckr {
            while self.nr < until {
                let (ckr, mk) = kdf_ck(d);
                *d = ckr;
                self.mkskipped.insert((self.hkr, self.nr), mk);
                self.nr += 1
            }
        }
        Ok(())
    }

    fn dhratchet(&mut self, header: &Header) {
        self.pn = self.ns;
        self.ns = 0;
        self.nr = 0;
        self.hks = self.nhks;
        self.hkr = self.nhkr;
        self.dhr = Some(header.public_key);
        let (rk, ckr, nhkr) = kdf_rk_he(&self.rk,
                                        &self.dhs.key_exchange(&self.dhr.unwrap()));
        self.rk = rk;
        self.ckr = Some(ckr);
        self.nhkr = Some(nhkr);
        self.dhs = DhKeyPair::new();
        let (rk, cks, nhks) = kdf_rk_he(&self.rk,
                                        &self.dhs.key_exchange(&self.dhr.unwrap()));
        self.rk = rk;
        self.cks = Some(cks);
        self.nhks = Some(nhks);
    }

    pub fn ratchet_decrypt(&mut self, enc_header: &(Vec<u8>, [u8; 24]), ciphertext: &[u8], nonce: &[u8; 24], ad: &[u8]) -> Result<Vec<u8>, RatchetError> {
        let ret = self.try_skipped_message_keys(enc_header, ciphertext, nonce, ad);
        if let Ok(d) = ret { return Ok(d.0) };
        let (header, dh_ratchet) = self.decrypt_header(enc_header)?;
        if dh_ratchet {
            self.skip_message_keys(header.pn).unwrap();
            self.dhratchet(&header);
        }
        self.skip_message_keys(header.n).unwrap();
        let (ckr, mk) = kdf_ck(&self.ckr.unwrap());
        self.ckr = Some(ckr);
        self.nr += 1;
        Ok(decrypt(&mk, ciphertext, &header.concat(ad)?, nonce)?)
    }

    pub fn ratchet_decrypt_w_header(&mut self, enc_header: &(Vec<u8>, [u8; 24]), ciphertext: &[u8], nonce: &[u8; 24], ad: &[u8]) -> Result<(Vec<u8>, Header), RatchetError> {
        let ret = self.try_skipped_message_keys(enc_header, ciphertext, nonce, ad);
        if let Ok(d) = ret { return Ok((d.0, d.1)) };
        let (header, dh_ratchet) = self.decrypt_header(enc_header).unwrap();
        if dh_ratchet {
            self.skip_message_keys(header.pn).unwrap();
            self.dhratchet(&header);
        }
        self.skip_message_keys(header.n).unwrap();
        let (ckr, mk) = kdf_ck(&self.ckr.unwrap());
        self.ckr = Some(ckr);
        self.nr += 1;
        Ok((decrypt(&mk, ciphertext, &header.concat(ad)?, nonce)?, header))
    }

    /// Export the ratchet to Binary data
    pub fn export(&self) -> Result<Vec<u8>, RatchetError> {
        let ex: ExRatchetEncHeader = self.into();
        match ex.try_to_vec() {
            Ok(v) => Ok(v),
            Err(_) => Err(RatchetError::SerializeDeserialize)
        }
    }

    /// Import the ratchet from Binary data. Panics when binary data is invalid.
    pub fn import(inp: &[u8]) -> Result<Self, RatchetError> {
        let ex: ExRatchetEncHeader = match ExRatchetEncHeader::try_from_slice(inp) {
            Ok(v) => v,
            Err(_) => return Err(RatchetError::SerializeDeserialize)
        };
        Ok(RatchetEncHeader::from(&ex))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn ratchet_init() {
        let sk = [1; 32];
        let (_bob_ratchet, public_key) = Ratchet::init_bob(sk);
        let _alice_ratchet = Ratchet::init_alice(sk, public_key);
    }

    #[test]
    fn ratchet_enc_single() {
        let sk = [1; 32];
        let (mut bob_ratchet, public_key) = Ratchet::init_bob(sk);
        let mut alice_ratchet = Ratchet::init_alice(sk, public_key);
        let data = include_bytes!("../src/header.rs").to_vec();
        let (header, encrypted, nonce) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted = bob_ratchet.ratchet_decrypt(&header, &encrypted, &nonce, b"").unwrap();
        assert_eq!(data, decrypted)
    }

    #[test]
    fn ratchet_enc_skip() {
        let sk = [1; 32];
        let (mut bob_ratchet, public_key) = Ratchet::init_bob(sk);
        let mut alice_ratchet = Ratchet::init_alice(sk, public_key);
        let data = include_bytes!("../src/header.rs").to_vec();
        let (header1, encrypted1, nonce1) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let (header2, encrypted2, nonce2) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let (header3, encrypted3, nonce3) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted3 = bob_ratchet.ratchet_decrypt(&header3, &encrypted3, &nonce3, b"").unwrap();
        let decrypted2 = bob_ratchet.ratchet_decrypt(&header2, &encrypted2, &nonce2, b"").unwrap();
        let decrypted1 = bob_ratchet.ratchet_decrypt(&header1, &encrypted1, &nonce1, b"").unwrap();
        let comp_res = decrypted1 == data && decrypted2 == data && decrypted3 == data;
        assert!(comp_res)
    }

    #[test]
    #[should_panic]
    fn ratchet_panic_bob() {
        let sk = [1; 32];
        let (mut bob_ratchet, _) = Ratchet::init_bob(sk);
        let data = include_bytes!("../src/header.rs").to_vec();
        let (_, _, _) = bob_ratchet.ratchet_encrypt(&data, b"").unwrap();
    }

    #[test]
    fn ratchet_encryt_decrypt_four() {
        let sk = [1; 32];
        let data = include_bytes!("../src/key.rs").to_vec();
        let (mut bob_ratchet, public_key) = Ratchet::init_bob(sk);
        let mut alice_ratchet = Ratchet::init_alice(sk, public_key);
        let (header1, encrypted1, nonce1) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted1 = bob_ratchet.ratchet_decrypt(&header1, &encrypted1, &nonce1, b"").unwrap();
        let (header2, encrypted2, nonce2) = bob_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted2 = alice_ratchet.ratchet_decrypt(&header2, &encrypted2, &nonce2, b"").unwrap();
        let comp_res = decrypted1 == data && decrypted2 == data;
        assert!(comp_res)
    }

    #[test]
    fn ratchet_ench_init() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (_bob_ratchet, public_key) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let _alice_ratchet = RatchetEncHeader::init_alice(sk, public_key,
                                                          shared_hka, shared_nhkb);
    }

    #[test]
    fn ratchet_ench_enc_single() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (mut bob_ratchet, public_key) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let mut alice_ratchet = RatchetEncHeader::init_alice(sk,
                                                             public_key,
                                                             shared_hka,
                                                             shared_nhkb);
        let data = include_bytes!("../src/header.rs").to_vec();
        let (header, encrypted, nonce) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted = bob_ratchet.ratchet_decrypt(&header, &encrypted, &nonce, b"").unwrap();
        assert_eq!(data, decrypted)
    }

    #[test]
    fn ratchet_ench_enc_skip() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (mut bob_ratchet, public_key) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let mut alice_ratchet = RatchetEncHeader::init_alice(sk,
                                                             public_key,
                                                             shared_hka,
                                                             shared_nhkb);
        let data = include_bytes!("../src/header.rs").to_vec();
        let (header1, encrypted1, nonce1) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let (header2, encrypted2, nonce2) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let (header3, encrypted3, nonce3) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted3 = bob_ratchet.ratchet_decrypt(&header3, &encrypted3, &nonce3, b"").unwrap();
        let decrypted2 = bob_ratchet.ratchet_decrypt(&header2, &encrypted2, &nonce2, b"").unwrap();
        let decrypted1 = bob_ratchet.ratchet_decrypt(&header1, &encrypted1, &nonce1, b"").unwrap();
        let comp_res = decrypted1 == data && decrypted2 == data && decrypted3 == data;
        assert!(comp_res)
    }

    #[test]
    #[should_panic]
    fn ratchet_ench_panic_bob() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (mut bob_ratchet, _) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let data = include_bytes!("../src/header.rs").to_vec();
        let (_, _, _) = bob_ratchet.ratchet_encrypt(&data, b"").unwrap();
    }

    #[test]
    fn ratchet_ench_decrypt_four() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (mut bob_ratchet, public_key) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let mut alice_ratchet = RatchetEncHeader::init_alice(sk, public_key, shared_hka, shared_nhkb);
        let data = include_bytes!("../src/key.rs").to_vec();
        let (header1, encrypted1, nonce1) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted1 = bob_ratchet.ratchet_decrypt(&header1, &encrypted1, &nonce1, b"").unwrap();
        let (header2, encrypted2, nonce2) = bob_ratchet.ratchet_encrypt(&data, b"").unwrap();
        let decrypted2 = alice_ratchet.ratchet_decrypt(&header2, &encrypted2, &nonce2, b"").unwrap();
        let comp_res = decrypted1 == data && decrypted2 == data;
        assert!(comp_res)
    }

    #[test]
    #[should_panic]
    fn ratchet_ench_enc_skip_panic() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (mut bob_ratchet, public_key) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let mut alice_ratchet = RatchetEncHeader::init_alice(sk,
                                                             public_key,
                                                             shared_hka,
                                                             shared_nhkb);
        let data = include_bytes!("../src/header.rs").to_vec();
        let mut headers = alloc::vec![];
        let mut encrypteds = alloc::vec![];
        let mut nonces = alloc::vec![];
        let mut decrypteds = alloc::vec![];
        for _ in 0..200 {
            let (header, encrypted, nonce) = alice_ratchet.ratchet_encrypt(&data, b"").unwrap();
            headers.push(header);
            encrypteds.push(encrypted);
            nonces.push(nonce);
        }
        headers.reverse();
        encrypteds.reverse();
        nonces.reverse();
        for idx in 0..200 {
            let header = headers.get(idx).unwrap();
            let encrypted = encrypteds.get(idx).unwrap();
            let nonce = nonces.get(idx).unwrap();
            let decrypted = bob_ratchet.ratchet_decrypt(header, encrypted, nonce, b"");
            decrypteds.push(decrypted);
        }
    }

    #[test]
    fn import_export() {
        let sk = [1; 32];
        let shared_hka = [2; 32];
        let shared_nhkb = [3; 32];
        let (bob_ratchet, public_key) = RatchetEncHeader::init_bob(sk,
                                                                       shared_hka,
                                                                       shared_nhkb);
        let alice_ratchet = RatchetEncHeader::init_alice(sk, public_key, shared_hka, shared_nhkb);

        let ex_bob_ratchet = bob_ratchet.export().unwrap();
        let in_bob_ratchet = RatchetEncHeader::import(&ex_bob_ratchet).unwrap();
        assert_eq!(in_bob_ratchet, bob_ratchet);

        let ex_alice_ratchet = alice_ratchet.export().unwrap();
        let in_alice_ratchet = RatchetEncHeader::import(&ex_alice_ratchet).unwrap();
        assert_eq!(in_alice_ratchet, alice_ratchet);
    }
}
